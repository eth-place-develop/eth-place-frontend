import ColorPicker from "simple-color-picker";

const canvasEl = document.getElementById("canvas");
const ctx = canvasEl.getContext("2d");
const zoomEl = document.getElementById("zoom");
const zoomtarget = document.getElementById("zoomtarget");
const zoomCtx = zoomEl.getContext("2d");
const zoomdragEl = document.getElementsByClassName("zoomdrag")[0];
const pickerEl = document.getElementsByClassName("color-picker")[0];

const state = {
  selectedColor: "#00B16A",
  pickerVisible: false,
};

const setState = (field, value) => {
  state[field] = value;
}

const setPixel = (ctx, x, y, hexColor) => {
  ctx.fillStyle = hexColor;
  ctx.fillRect(x, y, 1, 1);
}

const rgb2hex = (red, green, blue) => {
  const hex = x => ("0" + parseInt(x).toString(16)).slice(-2);
  return "#" + hex(red) + hex(green) + hex(blue)
}

const color = (clr, g, b) => {
  if (typeof g === "number") return rgb2hex(clr, g, b);
  if (typeof clr === "number") return rgb2hex(clr, clr, clr);
  return clr;
}

const getMousePos = (canvas, evt) => {
  const rect = canvas.getBoundingClientRect();
  return {
    x: Math.floor( ( evt.clientX - rect.left ) / ( rect.right - rect.left ) * canvas.width ),
    y: Math.floor( ( evt.clientY - rect.top ) / ( rect.bottom - rect.top ) * canvas.height )
  };
}

const colorFromArray = array => color(array[0], array[1], array[2]);

const drawRect = (ctx, x, y, width, height, color) => {
  ctx.fillStyle = color;
  ctx.fillRect(x, y, width, height);
}

const updateCanvas = () => {
  ctx.fillStyle = "#ffffff";
  ctx.fillRect(0, 0, 1000, 1000);

  data.map((v, i) => {
    const x = i % 1000;
    const y = Math.floor(i / 1000);
    setPixel(ctx, x, y, color(v));
  });
}

const setElPosition = (el, x, y) => {
  el.style.top = y + "px";
  el.style.left = x + "px";
}

const drawPixel = event => {
  const {x, y} = getMousePos(canvasEl, event);
  const coord = y * 1000 + x;
  data[coord] = state.selectedColor;
  setPixel(ctx, x, y, state.selectedColor);
  redrawZoom(event);
}

const showColorPicker = event => {

}

const show = el => el.classList.remove("hidden");
const hide = el => el.classList.add("hidden");

const hidePicker = () => {
  state.pickerVisible = false;
  hide(pickerEl);
};
const showPicker = () => {
  state.pickerVisible = true;
  show(pickerEl);
};

let refreshZoom = () => false;
let selectedX = 0;
let selectedY = 0;

canvasEl.addEventListener("click", event => {
  const {x, y} = getMousePos(canvasEl, event);
  showPicker();
  refreshZoom = () => {
    drawPixel(event);
    startTransaction(x, y, state.selectedColor);
  };
});

const redrawZoom = event => {
  const {x, y} = getMousePos(canvasEl, event);
  setElPosition(zoomdragEl, x + 10, y + 10);
  Array(11).fill().map( (v, _x) => {
    Array(10).fill().map( (v, _y) => {
      const data = ctx.getImageData(x + _x - 5, y + _y - 4, 1, 1).data;
      const fillColor = colorFromArray(data);
      drawRect(zoomCtx, _x * 10, _y * 10, 10, 10, fillColor);
    });
  });
};
canvasEl.addEventListener("mousemove", event => !state.pickerVisible ? redrawZoom(event) : null);

const initPicker = () => {
  var colorPicker = new ColorPicker({
    color: '#00B16A',
    background: '#333',
    el: document.getElementById("picker"),
    width: 170,
    height: 170
  });

  pickerEl.addEventListener("dragend", event => {
    const {screenX, screenY} = event;
    pickerEl.style.left = screenX + "px";
  });

  const colorInput = document.getElementById("color-text");

  const changeColor = newcolor => {
    colorInput.style.backgroundColor = newcolor;
    state.selectedColor = newcolor;
    zoomtarget.style.backgroundColor = newcolor;
  };

  colorPicker.onChange(newcolor => {
    colorInput.value = newcolor;
    changeColor(newcolor);
  });

  colorInput.addEventListener("change", event => {
    colorPicker.setColor(event.target.value);
    changeColor(event.target.value);
  });

  zoomEl.addEventListener("click", event => {
    const {x, y} = getMousePos(zoomEl, event);
    const data = zoomCtx.getImageData(x, y, 1, 1).data;
    const fillColor = colorFromArray(data);
    colorPicker.setColor(fillColor);
  });

  document.getElementById("cancelplacepixel").addEventListener("click", event => {
    zoomtarget.style.backgroundColor = undefined;
    hidePicker();
  });

  document.getElementById("yarrr").addEventListener("click", event => {
    refreshZoom();
    hidePicker();
  });
}
initPicker();

const generateRainbowArray = () => {
  const rndClr = () => Math.random() * 200 + 55;
  return Array(1000 * 30).fill()
    .map(() => 
      Math.random() < 0.01
        ? color(rndClr(), rndClr(), rndClr())
        : 255
    );
}

const data = [];
updateCanvas();

// Ethereum

const abi = [
	{
		"constant": false,
		"inputs": [
			{
				"name": "coordinates",
				"type": "uint256"
			},
			{
				"name": "color",
				"type": "uint16"
			}
		],
		"name": "setPixel",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "fieldAddr",
		"outputs": [
			{
				"name": "",
				"type": "address"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	}
];
const address = "0x357bf650385adF76719cDcdb90cC4b7a66f4a67B";
let contract = {};


const startTransaction = (x, y, color) => {
  const coord = x * 1000 + y;
  const intColor = parseInt(color.split("#").join(""), 16);
  const config = {
    gasPrice: 3000000000,
    gasLimit: 50000,
  };
  console.log("setPixel", 13, 15);
  contract = window.web3.eth.contract(abi).at(address);
  contract.setPixel.sendTransaction(13, 14, config, console.log);
}

if (typeof window.web3 !== 'undefined') {
  window.web3 = new Web3(web3.currentProvider);

  contract = window.web3.eth.contract(abi).at(address);

  const getRow = (rowNum, callback) =>
    contract.getColors.call(rowNum, (err, res) => 
      callback(rowNum, res.map(val => val.toNumber()))
    );
  
  const getRowByFrame = (start) => {
    if (start >= 400) return;
    Array(5).fill().map((v, i) => getRow(i + start, drawRow));
    requestAnimationFrame(() => getRowByFrame(start += 5) );
  }

  const decimalToHex = (d) => {
    var hex = Number(d).toString(16);
    hex = "000000".substr(0, 6 - hex.length) + hex; 
    return hex;
  }

  const drawRow = (rowNum, _data) => {
    _data.map((v, x) => {
      if(!v) return;
      setPixel(ctx, rowNum, x, "#" + decimalToHex(v));
    });
  }

  getRowByFrame(0);

  contract.allEvents({},{fromBlock: 0, toBlock: 'latest'}).watch(function(error, result){
    console.log("EVENT!");
    console.log(error);
    console.log(result);
  });

  var filter = web3.eth.filter("latest");
  filter.watch(function(error, result){
    console.log(error);
    console.log(result);
  });

} else {
  console.log('No web3. Sad.');
}