import Web3 from "web3";
import interact from "interactjs";
import settings from "./settings.js";
import "./Logo.js";

// Test NET
// settings.fieldContractAddress = "0xD4E03D2Dcfd4B91657A025BDe3B812439d932c92";
// settings.controllerContractAddress = "0x2cE90F032A24ddF2F665BE462febe797ab054A5C";

const canvasEl = document.getElementById("canvas");
const zoomdragEl = document.getElementsByClassName("zoomdrag")[0];
const zoomCtx = document.getElementById("zoom").getContext("2d");
const colorPickerEl = document.getElementsByClassName("color-picker")[0];
const pickerBlockerEl = document.querySelector(".send-blocker");
const ctx = canvasEl.getContext("2d");
const containerCanvas = document.querySelector(".container-canvas");
const noweb3popupEl = document.querySelector(".noweb3popup");
const metamaskPopupEl = document.querySelector(".metamask-popup");

const navigatorEl = document.querySelector(".navigator");
const navigatorContainerEl = document.querySelector(".navigator-container");
const navigatorCanvasEl = document.querySelector(".navigator-canvas");
const transactionStatusEl = document.querySelector(".transaction-status");
const stateUri = "https://static.ethplace.io/state.png";

let userPixels = [];
let isPickerVisible = false;
let fieldContract;
let controllerContract;
let isMetamaskWarning = false;
let isMetamask = false;


const initWithWeb3 = () => {
  const {
    fieldContractAddress,
    fieldABI,
    controllerContractAddress,
    controllerABI,
  } = settings;

  window.web3 = new Web3(window.web3.currentProvider);
  fieldContract = new window.web3.eth.Contract(fieldABI, fieldContractAddress);
  controllerContract = new window.web3.eth.Contract(controllerABI, controllerContractAddress);

  isMetamaskWarning = isMetamask = !!window.web3.currentProvider.isMetaMask;
  if (isMetamaskWarning) {
    document.body.classList.add("metamask-warning");
  }

  refreshCanvas();
  initPicker();
  initPalette();
  window.addEventListener("resize", drawNavigator);
  setInterval(refreshCanvas, 15 * 1000);
}

const initWithNOWeb3 = () => {
  refreshCanvas();
  initPicker(true);
  window.addEventListener("resize", drawNavigator);
  setInterval(refreshCanvas, 15 * 1000);
  document.body.classList.add("noweb3");
}

const drawNavigator = () => {
  const {width, height} = containerCanvas.getBoundingClientRect();
  const {offsetLeft, offsetTop} = canvasEl;
  navigatorCanvasEl.style.top  = offsetTop / 40 + "px";
  navigatorCanvasEl.style.left = offsetLeft / 40 + "px";
  navigatorCanvasEl.style.width  = settings.scaleSize * 25 + "px";
  navigatorCanvasEl.style.height = settings.scaleSize * 25 + "px";

  navigatorContainerEl.style.width  = width / 40 + "px";
  // navigatorEl.style.width  = width / 20 + "px";
  navigatorContainerEl.style.height = height / 40 + "px";
};

const decodeStringToArray = value => {
  return value.split("").reverse().map(v => parseInt(v, 16))
};

const drawArray = (array, startFrom) => {
  array.map((value, i) => {
    const {x, y} = numberToCoord(startFrom + i);
    setPixel(ctx, x, y, settings.colors[value]);
  });
}

const uriWithFragment = () => `${stateUri}?${new Date().getTime()}`;

const refreshCanvas = () => {
  window.img = new Image();

  const loadHandler = () => {
    ctx.drawImage(window.img, 0, 0, 1000, 1000, 0, 0, 1000, 1000);
    window.img.removeEventListener("load", loadHandler);
    drawNavigator();

    userPixels.map( value => {
      const {x, y} = numberToCoord(value.coord);
      setPixel(ctx, x, y, settings.colors[value.color]);
    });
  }

  window.img.addEventListener("load", loadHandler, false);
  window.img.onerror = event => {
    console.log("image loading error", event);
    setTimeout(() => {
      window.img.src = uriWithFragment();
    }, 500);
  }
  window.img.crossOrigin = "Anonymous";
  window.img.src = uriWithFragment();

  initZoom();

  // web3.eth.call({
  //   to: settings.fieldContractAddress,
  //   data: fieldContract.methods.allPixels().encodeABI()
  // }).then(result => {
  //   const pureResult = result.replace("0x", "");
  //   Array(15625).fill()
  //   .map((v, i) =>
  //     pureResult.substring(i * 64, i * 64 + 64))
  //   .map(decodeStringToArray)
  //   .map((v, i) => drawArray(v, i * 64));
  // }).then(initZoom);
}

const initZoom = () => {
  // Canvas Mouse MOVE
  canvasEl.addEventListener("mousemove", event => {
    if (isPickerVisible) return;
    const {x, y} = getMousePos(canvasEl, event);
    const {clientX, clientY} = event;
    const {innerHeight, innerWidth} = window;

    const rect = zoomdragEl.getBoundingClientRect();
    const zoomWidth = rect.width;
    const zoomHeight = rect.height;

    const normalizeX = ( (clientX + zoomWidth) > (innerWidth - 10) ) ? - zoomWidth - 20 : 0;
    const normalizeY = ( (clientY + zoomHeight) > (innerHeight - 10) ) ? - zoomHeight : 10;
    zoomdragEl.classList.remove("hidden");

    if (settings.scaleSize > 4) {
      zoomdragEl.style.top = event.clientY + window.pageYOffset + "px";
      zoomdragEl.style.left = event.clientX + window.pageXOffset + "px";
      return;
    }
    zoomdragEl.style.top = event.clientY + window.pageYOffset + 10 + normalizeY + "px";
    zoomdragEl.style.left = event.clientX + window.pageXOffset + 10 + normalizeX + "px";

    const data = ctx.getImageData(x - 10, y - 10, 20, 20).data;
    for (var i = 0; i < 400; i++) {
      const _x = i % 20;
      const _y = Math.floor(i / 20);
      const _data = data.slice(i * 4, i * 4 + 4);
      const fillColor = colorFromArray(_data);
      drawRect(zoomCtx, _x * 10, _y * 10, 10, 10, fillColor);
    }
  });
}

const initPicker = isNoWeb3 => {
  // Canvas Mouse OUT
  containerCanvas.addEventListener("mouseout", event => {
    // if (event.target !== containerCanvas) return;
    if (isPickerVisible) return;
    zoomdragEl.classList.add("hidden");
  });
  // Canvas Mouse CLICK
  let timeForClick = 0;
  canvasEl.addEventListener("mousedown", () => timeForClick = new Date());
  canvasEl.addEventListener("mouseup", event => {
    if ((new Date() - timeForClick) < 200) {
      isNoWeb3 ? showNoWeb3Popup() : showPicker(event);
    }
  });

  document.getElementById("cancelplacepixel")
    .addEventListener("click", hidePicker);
  pickerBlockerEl.addEventListener("click", hidePicker);
}

const showPicker = event => {
  const {x, y} = getMousePos(canvasEl, event);
  settings.selectedcoordinate = coordToNumber(x, y);
  colorPickerEl.classList.remove("hidden");
  isPickerVisible = true;
  if(isMetamaskWarning) showMetamaskPopup();
}

const showMetamaskPopup = () => {
  metamaskPopupEl.classList.remove("hidden");
  isMetamaskWarning = false;
}

metamaskPopupEl.addEventListener("click", evt => {
  metamaskPopupEl.classList.add("hidden");
  document.body.classList.remove("metamask-warning");
});

const showNoWeb3Popup = () => {
  noweb3popupEl.classList.remove("hidden");
}

const hidePicker = () => {
  colorPickerEl.classList.add("hidden");
  isPickerVisible = false;
}

const initPalette = () => {
  Array(16).fill().map((v, i) => {
    const div = document.querySelector(".color" + i);
    div.style.backgroundColor = settings.colors[i];
    div.addEventListener("click", colorSelected(i));
  });
}

const colorSelected = (color) => () => {
  hidePicker();
  web3.eth.getAccounts((_error, accounts) => {
    if (accounts.length === 0) {
      alert("Please login in you wallet. Account not found ¯\_(ツ)_/¯.");
      return;
    };
    const config = {
      from: accounts[0],
      gasPrice: isMetamask ? 3500000000 : 2500000000,
      gasLimit: 50000,
      value: 0,
    };

    controllerContract.methods.setPixel(settings.selectedcoordinate, color).send(config)
      .on("transactionHash", hash => {
        userPixels.push({
          coord: settings.selectedcoordinate,
          color: color
        });
        const {x, y} = numberToCoord(settings.selectedcoordinate);
        setPixel(ctx, x, y, settings.colors[color]);
        if (isMetamask) {
          document.body.classList.add("metamask");
          checkTransaction(hash);
          transactionStatusEl.setAttribute("href", `https://etherscan.io/tx/${hash}`);
        }
      })
      .on("receipt", receipt => {
        console.log("recipt", recipt);
        document.body.classList.remove("metamask");
      })
      .on("error", console.log);
  });
}
// ----->

const checkTransaction = hash => {
  const intervalID = window.setInterval(() => {
    web3.eth.getTransaction(hash, (err, result) => {
      if (result) {
        document.body.classList.remove("metamask");
        window.clearInterval(intervalID);
      }
    });
  }, 1500);
}

const coordToNumber = (x, y) => y * 1000 + x;
const numberToCoord = number => ({
  x: number % 1000,
  y: Math.floor(number / 1000),
});

const setPixel = (ctx, x, y, hexColor) => {
  ctx.fillStyle = hexColor;
  ctx.fillRect(x, y, 1, 1);
}

const getMousePos = (canvas, evt) => {
  const rect = canvas.getBoundingClientRect();
  return {
    x: Math.floor( ( evt.clientX - rect.left ) / ( rect.right - rect.left ) * canvas.width ),
    y: Math.floor( ( evt.clientY - rect.top ) / ( rect.bottom - rect.top ) * canvas.height )
  };
}

const rgb2hex = (red, green, blue) => {
  const hex = x => ("0" + parseInt(x).toString(16)).slice(-2);
  return "#" + hex(red) + hex(green) + hex(blue)
}

const colorFromArray = array => rgb2hex(array[0], array[1], array[2]);

const drawRect = (ctx, x, y, width, height, color) => {
  ctx.fillStyle = color;
  ctx.fillRect(x, y, width, height);
}

const pickerEl = document.querySelector(".color-picker");
pickerEl.addEventListener("dragend", event => {
  const {screenX, screenY} = event;
  pickerEl.style.left = screenX - 110 + "px";
});

const scaleCanvas = (scaleSize) => {
  const {top, left, width, height} = canvasEl.style;
  const canvasTop = parseInt(top) | 0;
  const canvasLeft = parseInt(left) | 0;
  const canvasWidth = parseInt(width);
  const canvasHeight = parseInt(height);

  const containerWidth = containerCanvas.clientWidth;
  const containerHeight = containerCanvas.clientHeight;

  const newLeftBase = ((canvasLeft - containerWidth / 2) / canvasWidth);
  const newTopBase = ((canvasTop - containerHeight / 2) / canvasHeight);

  canvasEl.style.width = scaleSize * 1000 + "px";
  canvasEl.style.height = scaleSize * 1000 + "px";

  const newCanvasWidth = parseInt(canvasEl.style.width);
  const newCanvasHeight = parseInt(canvasEl.style.height);
  const newLeft = newLeftBase * newCanvasWidth + containerWidth / 2 + "px";
  const newTop = newTopBase * newCanvasHeight + containerHeight / 2 + "px";

  canvasEl.classList.add("canvasAnimationResetPosition");
  window.setTimeout(() => {
    canvasEl.classList.remove("canvasAnimationResetPosition");
    drawNavigator();
  }, 500);

  canvasEl.style.left = newLeft;
  canvasEl.style.top = newTop;

  [...document.querySelectorAll(".scaleSelectorX")].map(el => el.classList.remove("selected"));
  document.querySelector(`div[data-x="${scaleSize}"]`).classList.add("selected");

  settings.scaleSize = scaleSize;

  (scaleSize > 4) ?
    document.body.classList.add("bigScale") :
    document.body.classList.remove("bigScale");
}

const normalizeCanvasHeight = () => {
  const windowHeight = window.innerHeight
    || document.documentElement.clientHeight
    || document.body.clientHeight;
  let containerCanvasHeight = windowHeight - 70;
  if (containerCanvasHeight < 550) containerCanvasHeight = 550;
  if (containerCanvasHeight > 1002) containerCanvasHeight = 1002;

  containerCanvas.style.height = containerCanvasHeight + "px";
}

// Init
window.addEventListener("load", e => {
  window.requestAnimationFrame(() => {
    if (typeof window.web3 == 'undefined') {
      console.log('no web3 way...');
      initWithNOWeb3();
    } else {
      initWithWeb3();
    }
  });
  //Init noWeb3Popup
  noweb3popupEl.addEventListener("click", event => {
    noweb3popupEl.classList.add("hidden");
  });

  // Init canvas scale
  document.querySelector(".scaleSelector").addEventListener("click", event => {
    const x = event.target.dataset.x;
    if (typeof x !== "string") return;
    scaleCanvas(x);
  });

  // Init canvas
  const draggableSettings = {
    onmove: event => {
      const left = (parseInt(canvasEl.style.left) | 0) + event.dx + "px";
      const top = (parseInt(canvasEl.style.top) | 0) + event.dy + "px";
      canvasEl.style.left = left;
      canvasEl.style.top = top;
      drawNavigator();
    },
    onend: event => {
      document.body.classList.add("hideHandCursor");
    }
  };

  interact("#canvas").draggable(draggableSettings).styleCursor(false);
  document.querySelector(".resetPosition").addEventListener("click", () => {
    scaleCanvas(1);
    canvasEl.classList.add("canvasAnimationResetPosition");
    window.setTimeout(() => canvasEl.classList.remove("canvasAnimationResetPosition"), 500);
    canvasEl.style.top = "0px";
    canvasEl.style.left = "0px";
  });
  normalizeCanvasHeight();
  window.addEventListener("resize", normalizeCanvasHeight);
});
